export interface User {
  _id: string;
  isOnline: boolean;
  name: string;
  avatar: string;
  lastName: string;
  username: string;
  email: string;
  phone: string;
  inscriptionDate: string;
  courses: {
    _id: string;
    title: string;
    description: string;
    percentCompleted: number;
    inscriptionDate: string;
  }[];
}
