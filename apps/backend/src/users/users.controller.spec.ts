import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User } from '../types/users';
import { HttpException, HttpStatus } from '@nestjs/common';
import { usersMock } from './usersMock';

describe('UsersController', () => {
  let usersController: UsersController;
  let usersService: UsersService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService],
    }).compile();

    usersController = app.get<UsersController>(UsersController);
    usersService = app.get<UsersService>(UsersService);
  });

  describe('findAll', () => {
    it('should return an array of users', () => {
      const result: User[] = usersMock;
      jest.spyOn(usersService, 'findAll').mockImplementation(() => result);
      expect(usersController.findAll()).toBe(result);
    });
  });

  describe('findOne', () => {
    it('should return a user with the specified id', () => {
      const user: User = usersMock[0];
      jest.spyOn(usersService, 'findOne').mockImplementation(() => user);
      expect(usersController.findOne('1')).toBe(user);
    });
  });

  describe('create', () => {
    it('should create a new user', () => {
      const user: User = usersMock[0];
      jest.spyOn(usersService, 'create').mockImplementation(() => {
        return;
      });
      expect(() => usersController.create(user)).not.toThrow();
    });

    it('should throw a HttpException with status 409 when the email already exists', () => {
      const user: User = usersMock[0];
      jest.spyOn(usersService, 'create').mockImplementation(() => {
        throw new Error(
          `El usuario con el correo electrónico ${user.email} ya existe`
        );
      });
      expect(() => usersController.create(user)).toThrow(
        new HttpException(
          `El usuario con el correo electrónico ${user.email} ya existe`,
          HttpStatus.CONFLICT
        )
      );
    });
  });
});
