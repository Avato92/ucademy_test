import { UsersService } from './users.service';
import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  HttpCode,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { User } from '../types/users';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  findAll(): Partial<User>[] {
    return this.usersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): User {
    try {
      return this.usersService.findOne(id);
    } catch (error) {
      throw new HttpException(
        `El usuario con el id ${id} no se ha encontrado`,
        HttpStatus.NOT_FOUND
      );
    }
  }

  @Post()
  @HttpCode(201)
  create(@Body() user: User): void {
    try {
      this.usersService.create(user);
    } catch (error) {
      throw new HttpException(
        `El usuario con el correo electrónico ${user.email} ya existe`,
        HttpStatus.CONFLICT
      );
    }
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() user: User): void {
    try {
      this.usersService.update(id, user);
    } catch (error) {
      throw new HttpException(
        `No se ha podido modiciar el usuario con id ${id}`,
        HttpStatus.NOT_FOUND
      );
    }
  }

  @Delete(':id')
  @HttpCode(204)
  delete(@Param('id') id: string): void {
    try {
      this.usersService.delete(id);
    } catch (error) {
      throw new HttpException(
        `No se ha podido eliminar el usuario con id ${id}`,
        HttpStatus.NOT_FOUND
      );
    }
  }
}
