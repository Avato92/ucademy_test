import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as fs from 'fs';
import * as path from 'path';
import { UsersService } from './users.service';
import { User } from '../types/users';
import { usersMock } from './usersMock';

describe('UsersService', () => {
  let usersService: UsersService;

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      providers: [UsersService],
    }).compile();
    usersService = moduleRef.get<UsersService>(UsersService);
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('findAll', () => {
    it('should return an array of users', () => {
      const users: User[] = usersMock;
      jest
        .spyOn(fs, 'readFileSync')
        .mockReturnValueOnce(Buffer.from(JSON.stringify(users)));

      const result = usersService.findAll();

      expect(result).toEqual(users);
    });
  });

  describe('findOne', () => {
    it('should return a user with the given id', () => {
      const users: User[] = usersMock;
      jest
        .spyOn(fs, 'readFileSync')
        .mockReturnValueOnce(Buffer.from(JSON.stringify(users)));

      const result = usersService.findOne('1');

      expect(result).toEqual(users[0]);
    });

    it('should throw a NotFoundException when the user is not found', () => {
      const users: User[] = usersMock;
      jest
        .spyOn(fs, 'readFileSync')
        .mockReturnValueOnce(Buffer.from(JSON.stringify(users)));

      expect(() => {
        usersService.findOne('999');
      }).toThrowError(NotFoundException);
    });
  });

  describe('create', () => {
    it('should add a user to the database', () => {
      const user: User = usersMock[0];
      const writeFileSyncMock = jest.spyOn(fs, 'writeFileSync');
      jest.spyOn(fs, 'readFileSync').mockReturnValueOnce(Buffer.from('[]'));

      usersService.create(user);

      expect(writeFileSyncMock).toHaveBeenCalledWith(
        expect.any(String),
        JSON.stringify([user]),
        'utf8'
      );
    });

    it('should throw an error when the user already exists', () => {
      const user: User = usersMock[0];
      jest
        .spyOn(fs, 'readFileSync')
        .mockReturnValueOnce(Buffer.from(JSON.stringify([user])));

      expect(() => {
        usersService.create(user);
      }).toThrowError(
        'El usuario con el correo electrónico Dovie_Howell79@gmail.com ya existe.'
      );
    });
  });
});
