import * as fs from 'fs';
import * as path from 'path';
import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common';
import { User } from '../types/users';

@Injectable()
export class UsersService {
  private parentDir = path.join(__dirname, '../../..');
  private readonly dbFilePath = path.join(this.parentDir, 'DB.json');

  private readDbFile(): User[] {
    const rawdata = fs.readFileSync(this.dbFilePath);
    return JSON.parse(rawdata.toString());
  }

  findAll(): Partial<User>[] {
    const users = this.readDbFile();
    return users.map((user) => ({
      _id: user._id,
      isOnline: user.isOnline,
      name: user.name,
      lastName: user.lastName,
      username: user.username,
      email: user.email,
      phone: user.phone,
    }));
  }

  findOne(id: string): User {
    const users = this.readDbFile();
    const user = users.find((user) => user._id === id);
    if (!user) {
      throw new NotFoundException(`User with id ${id} not found`);
    }
    return user;
  }

  create(user: User): void {
    const db = this.readDbFile();
    const existingUser = db.find((u) => u.email === user.email);
    if (existingUser) {
      throw new Error(
        `El usuario con el correo electrónico ${user.email} ya existe.`
      );
    }
    db.push(user);
    fs.writeFileSync(this.dbFilePath, JSON.stringify(db), 'utf8');
  }

  update(id: string, updateUserDto: User): void {
    const users = this.readDbFile();
    const index = users.findIndex((user) => user._id === id);
    if (index === -1) {
      throw new NotFoundException(`Usuario con id ${id} no encontrado`);
    }
    users[index] = { ...users[index], ...updateUserDto };
    fs.writeFileSync(this.dbFilePath, JSON.stringify(users));
  }

  delete(id: string): void {
    const users = this.readDbFile();
    const index = users.findIndex((user) => user._id === id);
    if (index === -1) {
      throw new NotFoundException(
        `No se puede eliminar el usuario con id ${id}, porque no se encuentra en la BD`
      );
    }
    users.splice(index, 1);
    fs.writeFileSync(this.dbFilePath, JSON.stringify(users));
  }
}
