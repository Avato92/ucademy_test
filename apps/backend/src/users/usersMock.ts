import { User } from '../types/users';

export const usersMock: User[] = [
  {
    _id: '1',
    isOnline: false,
    name: 'Maurine',
    avatar:
      'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/775.jpg',
    lastName: 'Harber',
    username: 'Clementine.Boyle',
    email: 'Dovie_Howell79@gmail.com',
    phone: '+34 6782 828 121',
    inscriptionDate: '02/06/2022',
    courses: [
      {
        _id: 'd102c6b4-ecc8-4733-9a4f-b51afd7f3826',
        title:
          'Ex dolorem voluptate ut suscipit et libero corrupti dolore quasi.',
        description:
          'Et culpa laboriosam. Necessitatibus dolor totam reprehenderit voluptas necessitatibus voluptatem aliquid delectus. Deserunt sequi modi. Et ad architecto.',
        percentCompleted: 48,
        inscriptionDate: '09/08/2022',
      },
      {
        _id: '3939d277-5dcf-41a7-a015-c46183be887d',
        title: 'Maxime magni odit qui porro occaecati accusantium.',
        description:
          'Earum eligendi odit ipsum. Accusamus et corporis rerum quod dignissimos repellendus omnis. Minus voluptas doloremque qui aut fuga. Et explicabo eveniet perferendis. Perferendis sunt quibusdam quis sint et tenetur saepe est.',
        percentCompleted: 91,
        inscriptionDate: '18/03/2022',
      },
      {
        _id: '30ebd893-0949-4765-8cab-0bb1af55b424',
        title:
          'Et esse tempora sed recusandae commodi consectetur necessitatibus voluptates mollitia.',
        description:
          'Voluptatem voluptas necessitatibus eveniet et deleniti qui. Iure eligendi dolore et. Explicabo est quibusdam possimus consequuntur tenetur beatae ipsa rerum animi. Asperiores itaque placeat. Laudantium et nemo aut quia qui ea consectetur. Tempore rerum est similique.',
        percentCompleted: 49,
        inscriptionDate: '02/04/2022',
      },
      {
        _id: '64717d0c-6638-4989-a2ff-82f46ea1a170',
        title:
          'Labore hic suscipit unde aut nam iusto consequuntur consectetur.',
        description:
          'Itaque aut facilis non illum officia. Eius quis illo quos quia. Molestiae aut eveniet quibusdam. Qui molestiae voluptatem.',
        percentCompleted: 91,
        inscriptionDate: '08/04/2022',
      },
      {
        _id: '2eef73d6-a192-4c3d-9134-ac417537d56f',
        title: 'Consectetur inventore blanditiis.',
        description:
          'Autem nostrum et adipisci sunt. Velit temporibus sit est explicabo doloribus sed fugit aut. Blanditiis laborum ipsam iure ad laborum aspernatur ex dolorum omnis. Asperiores eos ut et odit quis.',
        percentCompleted: 34,
        inscriptionDate: '13/04/2022',
      },
      {
        _id: '928807c8-c2b8-4930-b441-706338894bd0',
        title: 'Maxime debitis laborum accusamus et ut.',
        description:
          'Sit cupiditate libero et. Qui repellat nisi impedit ea molestiae et perferendis aut esse. Qui aut velit velit. Itaque quos qui sint et.',
        percentCompleted: 62,
        inscriptionDate: '08/01/2022',
      },
      {
        _id: '64b5c2b8-057f-42fc-86b7-6302e8f4672a',
        title: 'Incidunt odio et hic omnis et eum perspiciatis at.',
        description:
          'At non minus ut iste et non quibusdam esse. Illo sint voluptas ducimus quia consequatur natus quaerat qui fugit. Eos omnis soluta omnis alias. Nostrum qui et odio quisquam quia maiores sit.',
        percentCompleted: 95,
        inscriptionDate: '25/09/2022',
      },
    ],
  },
  {
    _id: '2',
    isOnline: true,
    name: 'Kameron',
    avatar:
      'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1222.jpg',
    lastName: 'Johnston',
    username: 'Arlie.Hamill',
    email: 'Yessenia_Reynolds7@yahoo.com',
    phone: '+34 6763 679 329',
    inscriptionDate: '24/03/2022',
    courses: [
      {
        _id: '2145c2d4-323f-4f30-952f-a2f74c21d14c',
        title: 'Error aut corrupti quos.',
        description:
          'Occaecati quos repellat maxime corporis deleniti eligendi optio. Est qui molestiae a laudantium. Blanditiis corporis ipsa fugit odit vel voluptatum corrupti architecto.',
        percentCompleted: 15,
        inscriptionDate: '26/05/2022',
      },
      {
        _id: 'ce411dff-148d-4aac-9747-0794994b2e5d',
        title: 'Impedit ea vel.',
        description:
          'Et magni adipisci qui sint similique incidunt ut illum. Reiciendis consequatur rerum dicta architecto. Autem et distinctio at. Natus tempora pariatur magni perferendis voluptate eum.',
        percentCompleted: 16,
        inscriptionDate: '07/10/2022',
      },
      {
        _id: '13499337-c574-48de-9d82-b0c02cbc4943',
        title: 'Nam aperiam aut.',
        description:
          'Corrupti accusantium qui. Cupiditate adipisci voluptatem harum laborum quo molestias. Dolores molestias aut.',
        percentCompleted: 87,
        inscriptionDate: '12/02/2022',
      },
      {
        _id: '1a2d4705-b749-472a-9209-e68fc57f0753',
        title: 'Qui blanditiis vitae est aperiam optio.',
        description:
          'Quam asperiores voluptatum quia impedit recusandae quae eveniet. Nam reiciendis sit ut ut alias rem earum. Deserunt laudantium ut in et dolorem atque repellendus officiis recusandae. Illo ut quia nobis expedita est est.',
        percentCompleted: 12,
        inscriptionDate: '24/03/2022',
      },
    ],
  },
];
