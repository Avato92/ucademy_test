import { Provider } from 'react-redux';
import { store } from '../store/index';
import Header from '../containers/Header/Header';
import TableContainer from '../containers/Table/TableContainer';
import GlobalStyles from '../styles/GlobalStyles';

export function App() {
  return (
    <Provider store={store}>
      <GlobalStyles />
      <Header />
      <TableContainer />
    </Provider>
  );
}

export default App;
