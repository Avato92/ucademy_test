import { MyButton, Text, IconWrapper } from './styles';
import { ButtonProps } from './types';

const Button = ({ primary, text, children }: ButtonProps) => {
  return (
    <MyButton primary={primary}>
      <IconWrapper>{children}</IconWrapper>
      <Text primary={primary}>{text}</Text>
    </MyButton>
  );
};

export default Button;
