import styled, { css } from 'styled-components';
import { MyButtonProps, TextProps } from './types';

const primaryButtonStyles = css`
  background-color: var(--color-text-white);
`;

const secondaryButtonStyles = css`
  background-color: var(--color-primary);
  border: 1px solid var(--color-primary);
  border-radius: 8px;
`;

const primaryTextStyles = css`
  color: var(--color-text-black);
`;

const secondaryTextStyles = css`
  color: var(--color-text-white);
`;

export const MyButton = styled.button<MyButtonProps>`
  display: flex;
  align-items: center;
  width: 10rem;
  height: 3.125rem;
  left: 1.3125rem;
  top: calc(50% - 1.5625rem - 22.25rem);
  border: none;
  ${({ primary }) => (primary ? primaryButtonStyles : secondaryButtonStyles)}
`;

export const Text = styled.span<TextProps>`
  width: 5.25rem;
  height: 1.375rem;
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 400;
  font-size: 0.9375rem;
  line-height: 1.375rem;
  color: #262d34;
  margin-left: 1rem;
  white-space: nowrap;
  padding-right: 0.5rem;
  ${({ primary }) => (primary ? primaryTextStyles : secondaryTextStyles)}
`;

export const IconWrapper = styled.span`
  display: flex;
  flex-shrink: 0;
  margin-right: 0.5rem;
`;
