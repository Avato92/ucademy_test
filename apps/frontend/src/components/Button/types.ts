import { ReactNode, ButtonHTMLAttributes } from 'react';

export interface ButtonProps {
  primary?: boolean;
  text: string;
  children: ReactNode;
}

export type MyButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  primary?: boolean;
};

export interface TextProps {
  primary?: boolean;
}
