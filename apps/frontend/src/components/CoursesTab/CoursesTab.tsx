import CourseProgress from '../CouseProgress/CouseProgress';
import { Course } from '../../types/types';

interface CoursesTabProps {
  courses: Course[];
}

const CoursesTab = ({ courses }: CoursesTabProps) => (
  <div>
    {courses.map((course) => (
      <CourseProgress course={course} key={course.title} />
    ))}
  </div>
);

export default CoursesTab;
