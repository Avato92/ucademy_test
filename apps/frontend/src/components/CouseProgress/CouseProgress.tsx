import { Course } from '../../types/types';
import { ProgressBar, Progress, CourseName, ProgressContainer } from './styles';

interface CourseProgressProps {
  course: Course;
}

const CourseProgress = ({ course }: CourseProgressProps) => {
  const { title, percentCompleted } = course;

  return (
    <div>
      <CourseName>{title}</CourseName>
      <ProgressContainer>
        <ProgressBar>
          <Progress style={{ width: `${percentCompleted}%` }} />
        </ProgressBar>
        <span>{`${percentCompleted}%`}</span>
      </ProgressContainer>
    </div>
  );
};

export default CourseProgress;
