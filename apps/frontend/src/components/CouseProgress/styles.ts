import styled from 'styled-components';

export const ProgressBar = styled.div`
  width: 90%;
  height: 12px;
  background-color: #ddd;
  border-radius: 5px;
  overflow: hidden;
`;

export const Progress = styled.div`
  height: 100%;
  background: linear-gradient(90deg, #0abb87 6.77%, #6fd466 93.23%);
`;

export const CourseName = styled.p`
  margin: 0 0 5px;
`;

export const ProgressContainer = styled.div`
  display: flex;
  align-items: center;

  & span {
    margin-left: 10px;
    font-weight: bold;
  }
`;
