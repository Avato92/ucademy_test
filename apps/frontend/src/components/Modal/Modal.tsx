import { useEffect, useState } from 'react';
import { ModalCloseButton, ModalWrapper, ModalContent } from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { handleModal, selectStudent } from '../../store/slices/studentsSlice';
import StudentsService from '../../services/StudentsService';
import { RootState } from '../../store';
import { Student } from '../../types/types';
import CoursesTab from '../CoursesTab/CoursesTab';
import MyModalHeader from '../ModalHeader/ModalHeader';
import ModalProfileTab from '../ModalProfileTab/ModalProfileTab';
import ModalUpdateTab from '../ModalUpdateTab/ModalUpdateTab';

export type tabEnum = 'profile' | 'courses' | 'update' | 'create';

const ModalUserDetails = () => {
  const [activeTab, setActiveTab] = useState<tabEnum>('profile');
  const [selectedStudent, setSelectedStudent] = useState<Student>({
    _id: '',
    isOnline: false,
    name: '',
    avatar: '',
    lastName: '',
    username: '',
    email: '',
    phone: '',
    inscriptionDate: '',
    courses: [],
  });
  const [student, setStudent] = useState<Partial<Student>>({
    name: '',
    lastName: '',
    username: '',
    email: '',
    phone: '',
  });
  const dispatch = useDispatch();
  const studentSelected = useSelector(
    (state: RootState) => state.students.studentSelected
  );
  const isOpen = useSelector((state: RootState) => state.students.isModalOpen);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setStudent((prevState) => ({ ...prevState, [name]: value }));
  };

  useEffect(() => {
    const fetchStudentInfo = async () => {
      try {
        const studentInfo = await StudentsService.getStudent(studentSelected);
        setSelectedStudent(studentInfo);
        setStudent(studentInfo);
      } catch (error) {
        console.error('Error al obtener información del estudiante', error);
      }
    };
    fetchStudentInfo();
  }, [studentSelected]);

  const handleTabClick = (tab: tabEnum) => {
    setActiveTab(tab);
  };

  const handleClick = () => {
    dispatch(selectStudent(''));
    dispatch(handleModal(!isOpen));
  };

  const updateStudent = async () => {
    StudentsService.updateStudent(selectedStudent._id, student);
    dispatch(selectStudent(''));
    dispatch(handleModal(!isOpen));
  };

  return (
    <ModalWrapper>
      <div className="modal-content">
        <MyModalHeader
          handleTabClick={handleTabClick}
          activeTab={activeTab}
          updateStudent={updateStudent}
        />
        <ModalContent>
          {activeTab === 'profile' && (
            <ModalProfileTab student={selectedStudent} />
          )}
          {activeTab === 'courses' && (
            <CoursesTab courses={selectedStudent.courses} />
          )}
          {(activeTab === 'update' || activeTab === 'create') && (
            <ModalUpdateTab student={student} handleChange={handleChange} />
          )}
        </ModalContent>

        <ModalCloseButton onClick={handleClick}>Cerrar</ModalCloseButton>
      </div>
    </ModalWrapper>
  );
};

export default ModalUserDetails;
