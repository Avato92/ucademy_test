import styled from 'styled-components';

export const ModalCloseButton = styled.button`
  padding: 5px 10px;
  margin-left: auto;
  background: #ffffff;
  border: 1px solid #262d34;
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
  border-radius: 8px;
`;

export const ModalWrapper = styled.div`
  position: fixed;
  z-index: 999;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;

  & .modal-content {
    width: 100%;
    max-width: 500px;
    max-height: 80%;
    overflow-y: auto;
    background-color: #fff;
    border-radius: 5px;
    padding: 20px;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
    display: flex;
    flex-direction: column;
  }
`;

export const ModalHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;
`;

export const ModalButton = styled.button<{ selected?: boolean }>`
  font-weight: ${({ selected }) => (selected ? 'bold' : 'normal')};
  background-color: transparent;
  border: none;
  border-bottom: ${({ selected }) => (selected ? '5px solid green' : 'none')};
  outline: none;
  cursor: pointer;
  font-size: 16px;
  transition: all 0.3s ease;
  margin-right: 10px;
  &:hover {
    background-color: ${({ selected }) =>
      selected ? '#f2f2f2' : 'rgba(255, 255, 255, 0.2)'};
  }
`;

export const ModalButtonContainer = styled.div`
  display: flex;
`;

export const ModalEditButton = styled.button`
  background-color: #262d34;
  color: #fff;
  border: none;
  outline: none;
  padding: 5px 20px;
  border-radius: 5px;
  cursor: pointer;
  font-size: 16px;
  transition: all 0.3s ease;
  margin-left: auto;
  font-family: 'Poppins';

  &:hover {
    background-color: #5a6268;
  }
`;

export const ModalCancelButton = styled.button`
  background: #ffffff;
  border: 1px solid #262d34;
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
  border-radius: 8px;
  transition: all 0.3s ease;
  margin-left: auto;
  font-family: 'Poppins';
  &:hover {
    background-color: #ccc;
  }
`;

export const ModalSaveButton = styled.button`
  background: #262d34;
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
  border-radius: 8px;
  &:hover {
    background-color: #5a6268;
  }
`;

export const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  overflow-y: auto;
`;

export const ModalImage = styled.img`
  width: 150px;
  height: 150px;
  object-fit: cover;
  border-radius: 50%;
  margin: 20px auto;
`;

export const ModalForm = styled.form`
  margin-bottom: 20px;
  display: flex;
  flex-direction: column;
  overflow: hidden;

  & label {
    margin-bottom: 5px;
    font-size: 16px;
    font-weight: bold;
  }

  & input {
    padding: 10px;
    font-size: 16px;
    outline: none;
    color: #000;
  }
`;

export const IconAndInput = styled.div`
  display: flex;
  align-items: center;
  & > div {
    margin-left: 10px;
  }
`;

export const WihtoutIconAndInput = styled.div`
  padding-left: 30px;
  display: flex;
  flex-direction: column;
`;

export const InputAndLabel = styled.div`
  display: flex;
  flex-direction: column;
  & > label {
    padding-left: 5px;
  }
`;

export const StyledInput = styled.input`
  background-color: transparent;
  border: none;
  border-bottom: 1px solid black;
`;
