import {
  ModalHeader,
  ModalButton,
  ModalButtonContainer,
  ModalEditButton,
  ModalCancelButton,
} from '../Modal/styles';
import { tabEnum } from '../Modal/Modal';

interface ModalHeaderProps {
  handleTabClick: (tab: tabEnum) => void;
  activeTab: tabEnum;
  updateStudent: () => void;
}

const MyModalHeader = ({
  handleTabClick,
  activeTab,
  updateStudent,
}: ModalHeaderProps) => {
  return (
    <ModalHeader>
      <ModalButtonContainer>
        <ModalButton
          selected={activeTab === 'profile'}
          onClick={() => handleTabClick('profile')}
        >
          Perfil
        </ModalButton>
        <ModalButton
          selected={activeTab === 'courses'}
          onClick={() => handleTabClick('courses')}
        >
          Cursos
        </ModalButton>
      </ModalButtonContainer>
      {activeTab !== 'update' ? (
        <ModalEditButton onClick={() => handleTabClick('update')}>
          Editar Estudiante
        </ModalEditButton>
      ) : (
        <>
          <ModalCancelButton onClick={() => handleTabClick('profile')}>
            Cancelar Edición
          </ModalCancelButton>
          <ModalEditButton onClick={() => updateStudent()}>
            Guardar
          </ModalEditButton>
        </>
      )}
    </ModalHeader>
  );
};

export default MyModalHeader;
