import {
  ModalForm,
  IconAndInput,
  WihtoutIconAndInput,
  InputAndLabel,
  StyledInput,
  ModalImage,
} from '../Modal/styles';
import UserIcon from '../../assets/icons/UserIcon';
import EmailIcon from '../../assets/icons/EmailIcon';
import MobileIcon from '../../assets/icons/MobileIcon';
import CalendarIcon from '../../assets/icons/CalendarIcon';
import { Student } from '../../types/types';

interface ModalProfileTabProps {
  student: Student;
}

const ModalProfileTab = ({ student }: ModalProfileTabProps) => {
  return (
    <>
      <ModalImage src={student.avatar} alt={student.name} />
      <ModalForm>
        <IconAndInput>
          <UserIcon />
          <InputAndLabel>
            <label htmlFor="name">Nombre y apellidos</label>
            <StyledInput type="text" id="name" value={student.name} readOnly />
          </InputAndLabel>
        </IconAndInput>
        <WihtoutIconAndInput>
          <label htmlFor="username">Nombre de usuario</label>
          <StyledInput
            type="text"
            id="username"
            value={student.username}
            readOnly
          />
        </WihtoutIconAndInput>
        <IconAndInput>
          <EmailIcon />
          <InputAndLabel>
            <label htmlFor="email">Email</label>
            <StyledInput
              type="email"
              id="email"
              value={student.email}
              readOnly
            />
          </InputAndLabel>
        </IconAndInput>
        <IconAndInput>
          <MobileIcon />
          <InputAndLabel>
            <label htmlFor="mobile">Móvil</label>
            <StyledInput
              type="text"
              id="mobile"
              value={student.phone}
              readOnly
            />
          </InputAndLabel>
        </IconAndInput>
        <IconAndInput>
          <CalendarIcon />
          <InputAndLabel>
            <label htmlFor="date">Fecha de inscripción</label>
            <StyledInput
              type="text"
              id="date"
              value={student.inscriptionDate}
              readOnly
            />
          </InputAndLabel>
        </IconAndInput>
      </ModalForm>
    </>
  );
};

export default ModalProfileTab;
