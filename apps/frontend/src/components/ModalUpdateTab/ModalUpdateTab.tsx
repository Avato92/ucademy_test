import styled from 'styled-components';
import { Student } from '../../types/types';

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;
`;

const Label = styled.label`
  margin-bottom: 8px;
`;

const Input = styled.input`
  background: #ffffff;
  border: 1px solid #c9c9c9;
  border-radius: 5px;
  padding: 8px;
  margin-bottom: 16px;
  width: ${(props) => props.width};
`;

const NameInputs = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 16px;
`;

const FormFieldContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

interface FormFieldProps {
  label: string;
  name: string;
  type: string;
  fullWidth?: string;
  value: string;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const FormField = ({
  label,
  name,
  type,
  fullWidth = '100%',
  value,
  handleChange,
}: FormFieldProps) => {
  return (
    <FormFieldContainer>
      <Label htmlFor={name}>{label}</Label>
      <Input
        type={type}
        id={name}
        name={name}
        width={fullWidth}
        value={value}
        onChange={handleChange}
      />
    </FormFieldContainer>
  );
};

interface FormPageProps {
  student: Partial<Student>;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const FormPage = ({ student, handleChange }: FormPageProps) => {
  return (
    <Form>
      <NameInputs>
        <FormField
          label="Nombre"
          name="name"
          type="text"
          value={student.name || ''}
          handleChange={handleChange}
        />
        <FormField
          label="Apellidos"
          name="lastName"
          type="text"
          value={student.lastName || ''}
          handleChange={handleChange}
        />
      </NameInputs>
      <FormField
        label="Nombre de usuario"
        name="username"
        type="text"
        fullWidth="100%"
        value={student.username || ''}
        handleChange={handleChange}
      />
      <FormField
        label="Email"
        name="email"
        type="email"
        fullWidth="100%"
        value={student.email || ''}
        handleChange={handleChange}
      />
      <FormField
        label="Móvil"
        name="mobile"
        type="text"
        value={student.phone || ''}
        handleChange={handleChange}
      />
    </Form>
  );
};

export default FormPage;
