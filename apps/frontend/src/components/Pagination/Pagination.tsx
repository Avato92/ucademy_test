import styled from 'styled-components';

interface PaginationProps {
  handleNextPage: () => void;
  handlePrevPage: () => void;
  currentPage: number;
  totalPages: number;
}

const PaginationWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 16px;

  button {
    background-color: #007bff;
    color: var(--color-text-white);
    border: none;
    border-radius: 4px;
    padding: 8px 16px;
    margin: 0 8px;
    cursor: pointer;
    transition: background-color 0.3s ease;

    &:hover {
      background-color: #0062cc;
    }

    &:disabled {
      opacity: 0.5;
      cursor: default;
    }
  }

  span {
    margin: 0 16px;
    font-size: 18px;
    font-weight: bold;
  }
`;

const Pagination = ({
  handleNextPage,
  handlePrevPage,
  currentPage,
  totalPages,
}: PaginationProps) => {
  return (
    <PaginationWrapper>
      <button onClick={handlePrevPage} disabled={currentPage === 1}>
        Anterior
      </button>
      <span>{`Página ${currentPage} de ${totalPages}`}</span>
      <button onClick={handleNextPage} disabled={currentPage === totalPages}>
        Siguiente
      </button>
    </PaginationWrapper>
  );
};

export default Pagination;
