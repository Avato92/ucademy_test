import styled from 'styled-components';

interface StatusProps {
  isOnline: boolean;
}

const StatusBadge = styled.span<StatusProps>`
  display: inline-block;
  padding: 4px 8px;
  border-radius: 4px;
  font-size: 14px;
  font-weight: bold;
  color: var(--color-text-white);
  background-color: ${(props) =>
    props.isOnline ? 'var(--color-primary)' : 'var(--color-gray)'};
`;

export default StatusBadge;
