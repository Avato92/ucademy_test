import { useSelector } from 'react-redux';
import Pagination from '../Pagination/Pagination';
import usePaginatedStudents from '../../hooks/useFetchStudents';
import { MyTable, TableWrapper } from './styles';
import { PAGE_SIZE } from '../../utils/constants';
import TableHeader from '../TableHeader/TableHeader';
import TableContent from '../TableContent/TableContent';
import TableError from '../TableError/TableError';
import TableLoading from '../TableLoading/TableLoading';
import { columns } from './constants';
import { RootState } from '../../store';
import Modal from '../../containers/Modal/Modal';
import ModalUserDetails from '../Modal/Modal';

const Table: React.FC = () => {
  const isOpen = useSelector((state: RootState) => state.students.isModalOpen);

  const [
    paginatedStudents,
    isLoading,
    error,
    totalPages,
    currentPage,
    handlePrevPage,
    handleNextPage,
  ] = usePaginatedStudents(PAGE_SIZE);

  return (
    <TableWrapper>
      <MyTable>
        {isLoading ? (
          <TableLoading />
        ) : error ? (
          <TableError error={error} />
        ) : (
          <>
            <TableHeader columns={columns} />
            <TableContent data={paginatedStudents} columns={columns} />
          </>
        )}
      </MyTable>
      {!isLoading && (
        <Pagination
          handleNextPage={handleNextPage}
          handlePrevPage={handlePrevPage}
          currentPage={currentPage}
          totalPages={totalPages}
        />
      )}
      {isOpen && (
        <Modal>
          <ModalUserDetails />
        </Modal>
      )}
    </TableWrapper>
  );
};

export default Table;
