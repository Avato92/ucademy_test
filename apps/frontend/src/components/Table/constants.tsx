import StatusBadge from '../StatusBadge/StatusBadge';
import InfoIcon from '../../assets/icons/InfoIcon';
import { Student } from '../../types/types';
import { ColumnType } from './types';

export const columns: ColumnType[] = [
  {
    key: 'isOnline',
    title: 'Conexión',
    render: (row: Student) =>
      row.isOnline ? (
        <StatusBadge isOnline>Online</StatusBadge>
      ) : (
        <StatusBadge isOnline={row.isOnline}>Offline</StatusBadge>
      ),
  },
  {
    key: 'name',
    title: 'Nombre y apellidos',
    render: (row: Student) => `${row.name} ${row.lastName}`,
  },
  {
    key: 'username',
    title: 'Nombre de usuario',
    render: (row: Student) => row.username,
  },
  { key: 'email', title: 'Email', render: (row: Student) => row.email },
  {
    key: 'phone',
    title: 'Móvil',
    render: (row: Student) => row.phone.replace(/\s/g, '').slice(-10),
  },
  {
    key: 'info',
    title: '',
    render: (row: Student) => <InfoIcon id={row._id} />,
  },
];
