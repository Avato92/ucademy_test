import styled from 'styled-components';

export const TableWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 2rem auto;
  max-width: 800px;
`;

export const MyTable = styled.table`
  width: 100%;
  border-collapse: collapse;
  border: none;

  @media screen and (min-width: 601px) {
    width: 100rem;
  }
`;

export const TableBody = styled.tbody``;

export const TableRow = styled.tr``;

export const TableCell = styled.td`
  padding: 1rem;
  border-bottom: 1px solid #cdcdcd;
`;
