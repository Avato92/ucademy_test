import { Student } from '../../types/types';

export interface ColumnType {
  key: string;
  title: string;
  render: (row: Student) => React.ReactNode;
}
