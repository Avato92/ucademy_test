import { Student } from '../../types/types';
import { TableCell, TableRow } from '../Table/styles';
import { ColumnType } from '../Table/types';

const TableContent: React.FC<{ data: Student[]; columns: ColumnType[] }> = ({
  data,
  columns,
}) => {
  return (
    <tbody>
      {data.map((row) => (
        <TableRow key={row._id}>
          {columns.map(({ key, render }) => (
            <TableCell key={key}>{render(row)}</TableCell>
          ))}
        </TableRow>
      ))}
    </tbody>
  );
};

export default TableContent;
