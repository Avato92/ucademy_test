import { TableRow, TableCell } from '../Table/styles';
import { TableErrorProps } from './types';

const TableError: React.FC<TableErrorProps> = ({ error }) => {
  return (
    <tbody>
      <TableRow>
        <TableCell>{error}</TableCell>
      </TableRow>
    </tbody>
  );
};

export default TableError;
