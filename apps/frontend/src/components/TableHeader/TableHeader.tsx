import { TableRow } from '../Table/styles';
import { TableHead, TableHeaderCell } from './styles';
import { Student } from '../../types/types';

type Column = {
  key: string;
  title: string;
  render: (row: Student) => React.ReactNode;
};

type TableHeaderProps = {
  columns: Column[];
};

const TableHeader: React.FC<TableHeaderProps> = ({ columns }) => {
  return (
    <TableHead>
      <TableRow>
        {columns.map(({ key, title }) => (
          <TableHeaderCell key={key}>{title}</TableHeaderCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export default TableHeader;
