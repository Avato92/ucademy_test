import styled from 'styled-components';

export const TableHead = styled.thead`
  background-color: transparent;
  border: none;
  border-bottom: 2px solid #262d34;
`;

export const TableHeaderCell = styled.th`
  padding: 0.5rem;
  text-align: left;
  font-weight: bold;
`;
