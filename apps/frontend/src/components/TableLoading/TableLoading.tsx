import { TableRow, TableCell } from '../Table/styles';

const TableLoading: React.FC = () => {
  return (
    <tbody>
      <TableRow>
        <TableCell>Loading...</TableCell>
      </TableRow>
    </tbody>
  );
};

export default TableLoading;
