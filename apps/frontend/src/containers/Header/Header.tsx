import styled from 'styled-components';
import Logo from '../../assets/icons/Logo';

const StyledHeader = styled.header`
  position: relative;
  width: 100%;
  height: 97px;
  left: 0px;
  top: 0px;
  background: #262d34;
  border-radius: 0px;
`;

const Header = () => {
  return (
    <StyledHeader>
      <Logo />
    </StyledHeader>
  );
};
export default Header;
