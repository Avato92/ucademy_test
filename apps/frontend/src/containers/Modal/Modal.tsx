import { useDispatch, useSelector } from 'react-redux';
import { ModalWrapper, ModalCloseButton } from '../../components/Modal/styles';
import { RootState } from '../../store';
import { handleModal } from '../../store/slices/studentsSlice';

interface ModalProps {
  children: React.ReactNode;
}

const Modal = ({ children }: ModalProps) => {
  const dispatch = useDispatch();

  const isOpen = useSelector((state: RootState) => state.students.isModalOpen);

  const handleClick = () => {
    dispatch(handleModal(!isOpen));
  };

  return (
    <ModalWrapper>
      {children}
      <ModalCloseButton onClick={handleClick}>Cerrar</ModalCloseButton>
    </ModalWrapper>
  );
};

export default Modal;
