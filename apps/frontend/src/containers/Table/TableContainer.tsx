import styled from 'styled-components';
import DashboardIcon from '../../assets/icons/DashboardIcon';
import PlusIcon from '../../assets/icons/PlusIcon';
import Button from '../../components/Button/Button';
import Table from '../../components/Table/Table';

const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  & > * + * {
    margin-left: 4rem;
  }
  margin: 2.5rem 0 0 2rem;
`;

const TableContainer = () => {
  return (
    <>
      <ButtonContainer>
        <Button primary text={'Dashboard'}>
          <DashboardIcon />
        </Button>
        <Button text={'Nuevo estudiante'}>
          <PlusIcon />
        </Button>
      </ButtonContainer>
      <Table />
    </>
  );
};

export default TableContainer;
