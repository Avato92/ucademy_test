import { useState, useEffect, useMemo } from 'react';
import { Student } from '../types/types';
import StudentsService from '../services/StudentsService';
import { useSelector } from 'react-redux';
import { RootState } from '../store';

const useFetchStudents = (): [Student[], boolean, string] => {
  const [students, setStudents] = useState<Student[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState('');
  const studentSelected = useSelector(
    (state: RootState) => state.students.studentSelected
  );

  useEffect(() => {
    const fetchStudents = async () => {
      try {
        const data = await StudentsService.getAll();
        setStudents(data);
        setIsLoading(false);
      } catch (error) {
        setError('Error al obtener los estudiantes');
        setIsLoading(false);
      }
    };

    fetchStudents();
  }, [studentSelected]);

  return [students, isLoading, error];
};

const usePaginatedStudents = (
  pageSize: number
): [Student[], boolean, string, number, number, () => void, () => void] => {
  const [students, isLoading, error] = useFetchStudents();
  const [currentPage, setCurrentPage] = useState(1);
  const totalPages = Math.ceil(students.length / pageSize);

  const paginatedStudents = useMemo(() => {
    if (!isLoading && !error) {
      const startIndex = (currentPage - 1) * pageSize;
      const endIndex = startIndex + pageSize;
      return students.slice(startIndex, endIndex);
    } else {
      return [];
    }
  }, [students, isLoading, error, currentPage, pageSize]);

  const goToPage = (page: number) => {
    setCurrentPage(page);
  };

  const handlePrevPage = () => {
    goToPage(currentPage - 1);
  };

  const handleNextPage = () => {
    goToPage(currentPage + 1);
  };

  return [
    paginatedStudents,
    isLoading,
    error,
    totalPages,
    currentPage,
    handlePrevPage,
    handleNextPage,
  ];
};

export default usePaginatedStudents;
