import axios from 'axios';
import { Student } from '../types/types';
import { REACT_APP_STUDENTS_API } from '../utils/constants';

const StudentsService = {
  getAll: async (): Promise<Student[]> => {
    const response = await axios.get(REACT_APP_STUDENTS_API);
    return response.data;
  },

  getStudent: async (id: string): Promise<Student> => {
    const response = await axios.get(`${REACT_APP_STUDENTS_API}/${id}`);
    return response.data;
  },

  updateStudent: async (id: string, body: Partial<Student>): Promise<void> => {
    try {
      await axios.put(`${REACT_APP_STUDENTS_API}/${id}`, body);
    } catch (error) {
      console.error(error);
    }
  },
};

export default StudentsService;
