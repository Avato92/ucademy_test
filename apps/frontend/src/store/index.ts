import { configureStore } from '@reduxjs/toolkit';
import studentsSlice from './slices/studentsSlice';

export const store = configureStore({
  reducer: {
    students: studentsSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
