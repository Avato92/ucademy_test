import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Student } from '../../types/types';

interface StudentsState {
  students: Student[];
  studentSelected: string;
  isModalOpen: boolean;
}

const initialState: StudentsState = {
  students: [],
  studentSelected: '',
  isModalOpen: false,
};

const studentsSlice = createSlice({
  name: 'students',
  initialState,
  reducers: {
    setStudents: (state, action: PayloadAction<Student[]>) => {
      state.students = action.payload;
    },
    addStudent: (state, action: PayloadAction<Student>) => {
      state.students.push(action.payload);
    },
    updateStudent: (state, action: PayloadAction<Student>) => {
      const index = state.students.findIndex(
        (student) => student._id === action.payload._id
      );
      if (index !== -1) {
        state.students[index] = action.payload;
      }
    },
    deleteStudent: (state, action: PayloadAction<string>) => {
      state.students = state.students.filter(
        (student) => student._id !== action.payload
      );
    },
    selectStudent: (state, action: PayloadAction<string>) => {
      state.studentSelected = action.payload;
    },
    handleModal: (state, action: PayloadAction<boolean>) => {
      state.isModalOpen = action.payload;
    },
  },
});

export const {
  setStudents,
  addStudent,
  updateStudent,
  deleteStudent,
  selectStudent,
  handleModal,
} = studentsSlice.actions;

export default studentsSlice;
