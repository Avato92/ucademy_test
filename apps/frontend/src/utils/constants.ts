/** Simulates an .env file */
export const REACT_APP_STUDENTS_API = 'http://localhost:3333/api/users';

/** Other variables */
export const PAGE_SIZE = 10;
